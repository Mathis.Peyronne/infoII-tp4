class File:
    def __init__(self):
        self.items = []

    def enqueue(self, item):
        priority = item[0]
        index = 0
        while index < len(self.items) and self.items[index][0] <= priority:
            index += 1
        self.items.insert(index, item)

    def dequeue(self):
        if self.is_empty():
            return None
        else:
            return self.items.pop(0)

    def is_empty(self):
        return (self.items == [])

    def size(self):
        return len(self.items)

file_prioritaire = File()

file_prioritaire.enqueue((4, "Nathan"))
file_prioritaire.enqueue((2, "Julia"))
file_prioritaire.enqueue((1, "Amandine"))
file_prioritaire.enqueue((3, "Mathias"))

print("File prioritaire:")
while not file_prioritaire.is_empty():
    print(file_prioritaire.dequeue())
